// +build acceptance

package acceptance_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"github.com/stretchr/testify/assert"
)

type config struct {
	project          string
	serviceName      string
	bucketName       string
	subscriptionName string
}

var testConfig config

func TestMain(m *testing.M) {
	// Set up our acceptance test configuration. These values are
	// required, so we will use a helper function that exits if the
	// environment variable is not set.
	testConfig = config{
		project:          getConfigurationValue("PROJECT"),
		serviceName:      getConfigurationValue("SERVICE_NAME"),
		bucketName:       getConfigurationValue("BUCKET"),
		subscriptionName: getConfigurationValue("SUBSCRIPTION"),
	}
	os.Exit(m.Run())
}

func TestSimpleHappyPath(t *testing.T) {
	t.Logf("CONFIGURATION: %+v\n", testConfig)

	// ARRANGE
	// Setup clients to talk to GCP resources.
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	bkt := client.Bucket(testConfig.bucketName)
	pubsubCtx, cancelCtx := context.WithTimeout(ctx, 2*time.Minute)
	subscription := setupSubscription(t, pubsubCtx)

	// Create a test message.
	testMsg := map[string]string{"message": "Ruby is awesome!!!"}

	// ACT
	// Post test message.
	resp := postTestMessage(t, testMsg)

	// ASSERT
	// Validate the response status code.
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)

	// Read and validate the response body.
	defer resp.Body.Close()
	rawBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	objectName := getObjectNameFromBytes(t, rawBody)

	t.Run("message is sent to PubSub topic", func(t *testing.T) {
		// Receive message from the subscription.
		var receivedBytes []byte
		err := subscription.Receive(pubsubCtx, func(ctx context.Context, m *pubsub.Message) {
			receivedBytes = m.Data
			m.Ack()
			cancelCtx()
		})
		assert.NoError(t, err)

		// Validate received message and the the object name matches
		// the object name previously return from the service.
		receivedObjectName := getObjectNameFromBytes(t, receivedBytes)
		assert.Equal(t, objectName, receivedObjectName)
	})

	t.Run("message is stored in bucket", func(t *testing.T) {
		// Check the stored message is the one we sent.
		storedMsg := readStoredObject(ctx, t, bkt, objectName)
		assert.EqualValues(t, testMsg, storedMsg)
	})
	// Clean up after our test. Delete the object so we can delete the bucket.
	err = bkt.Object(objectName).Delete(ctx)
	assert.NoError(t, err)
}

func TestSimpleSadPath(t *testing.T) {
	t.Run("posting a incorrectly formatted message returns 400", func(t *testing.T) {
		resp := postTestMessage(t, "this wont unmarshal")
		assert.Equal(t, http.StatusBadRequest, resp.StatusCode)
	})
}

func getConfigurationValue(envVar string) string {
	value := os.Getenv(envVar)
	if value == "" {
		log.Fatalf("%s not set", envVar)
	}
	return value
}

func getObjectNameFromBytes(t *testing.T, bytes []byte) string {
	assert.NotNil(t, bytes, "nil bytes")
	body := make(map[string]string)
	err := json.Unmarshal(bytes, &body)
	assert.NoError(t, err)

	objectName := body["name"]
	assert.NotZero(t, objectName)
	return objectName
}

func setupSubscription(t *testing.T, ctx context.Context) *pubsub.Subscription {
	pubsubClient, err := pubsub.NewClient(ctx, testConfig.project)
	assert.NoError(t, err)
	sub := pubsubClient.Subscription(testConfig.subscriptionName)
	return sub
}

func postTestMessage(t *testing.T, msg interface{}) *http.Response {
	url := fmt.Sprintf("http://%s/message", testConfig.serviceName)
	bodyBytes, err := json.Marshal(msg)
	assert.NoError(t, err)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(bodyBytes))
	assert.NoError(t, err)
	return resp
}

func readStoredObject(ctx context.Context, t *testing.T, bkt *storage.BucketHandle, objectName string) map[string]string {
	reader, err := bkt.Object(objectName).NewReader(ctx)
	assert.NoError(t, err)

	defer reader.Close()
	bytes, err := ioutil.ReadAll(reader)
	assert.NoError(t, err)

	storedMsg := make(map[string]string)
	err = json.Unmarshal(bytes, &storedMsg)
	assert.NoError(t, err)
	t.Logf("Stored message: %+v", storedMsg)
	return storedMsg
}
