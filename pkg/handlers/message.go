package handlers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/rocore/demo-app/pkg/apitype"
)

// MessageHandler returns a handler that stores the messages it receives.
func MessageHandler(topic *pubsub.Topic, bkt *storage.BucketHandle) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		// Read the request body.
		defer r.Body.Close()
		msgData, err := ioutil.ReadAll(r.Body)
		if err != nil {
			handleServerError(w, err, "error retrieving body")
			return
		}

		// Unmarshal the request body bytes into the inbound request,
		// which is a simple JSON object with a message field. If we
		// cannot unmarshal the request, we return a 400 to the client.
		var inboundRequest apitype.InboundRequest
		err = json.Unmarshal(msgData, &inboundRequest)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Write the inbound request to our bucket.
		objectName, err := writeToBucket(ctx, bkt, inboundRequest)
		if err != nil {
			handleServerError(w, err, "error writing to bucket")
			return
		}

		// Generate the outbound message which includes the
		// object name where we stored the new message.
		outboundMsg := apitype.NewMessageAlert{Name: objectName}
		outboundBytes, err := json.Marshal(outboundMsg)
		if err != nil {
			handleServerError(w, err, "error marshalling bytes")
			return
		}

		// Send a notification that we received a new message.
		err = writeToPubSub(ctx, topic, outboundBytes)
		if err != nil {
			handleServerError(w, err, "error writing to pubsub")
			return
		}

		// Write the response to client.
		w.WriteHeader(http.StatusAccepted)
		w.Write(outboundBytes)
	}
}

func handleServerError(w http.ResponseWriter, err error, msg string) {
	log.Printf("%s: %v", msg, err)
	w.WriteHeader(http.StatusInternalServerError)
}

func writeToBucket(ctx context.Context, bkt *storage.BucketHandle, inboundRequest apitype.InboundRequest) (string, error) {
	log.Println("writing to bucket")
	objectName := uuid.NewV4().String()
	obj := bkt.Object(objectName)
	w := obj.NewWriter(ctx)
	defer w.Close()

	bytes, err := json.Marshal(inboundRequest)
	if err != nil {
		return "", errors.Wrap(err, "unable to marshal data")
	}
	if _, err := w.Write(bytes); err != nil {
		return "", errors.Wrap(err, "unable to write data to bucket")
	}
	return objectName, nil
}

func writeToPubSub(ctx context.Context, topic *pubsub.Topic, outboundBytes []byte) error {
	log.Println("writing to PubSub")
	msg := pubsub.Message{Data: outboundBytes}
	result := topic.Publish(ctx, &msg)
	if _, err := result.Get(ctx); err != nil {
		return errors.Wrap(err, "unable to write data to PubSub")
	}
	return nil
}
