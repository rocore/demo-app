package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	"gitlab.com/rocore/demo-app/pkg/handlers"
)

func main() {
	ctx := context.Background()

	// Get configuration from environment variables. These are
	// required configuration values, so we will use an helper
	// function get the values and exit if the value is not set.
	project := getConfigurationValue("PROJECT")
	topicName := getConfigurationValue("TOPIC")
	bucketName := getConfigurationValue("BUCKET")

	// Set up the GCP resources we need.
	topic := getPubSubTopic(ctx, project, topicName)
	bkt := getBucketHandle(ctx, bucketName)

	// Create two handlers. One is a basic health endpoint and
	// the other is the handler we will be testing.
	mux := http.NewServeMux()
	mux.HandleFunc("/ping", handlers.PingHandler)
	mux.HandleFunc("/message", handlers.MessageHandler(topic, bkt))

	// Start server.
	log.Println("starting server")
	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Fatalf("error listening on localhost:8080: %v", err)
	}
}

func getConfigurationValue(envVar string) string {
	value := os.Getenv(envVar)
	if value == "" {
		log.Fatalf("%s not set", envVar)
	}
	log.Printf("%s: %s", envVar, value)
	return value
}

func getPubSubTopic(ctx context.Context, project, topicName string) *pubsub.Topic {
	pubsubClient, err := pubsub.NewClient(ctx, project)
	if err != nil {
		log.Fatalf("cannot create pubsub client: %v", err)
	}
	return pubsubClient.Topic(topicName)
}

func getBucketHandle(ctx context.Context, bucketName string) *storage.BucketHandle {
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("cannot create storage client: %v", err)
	}
	return storageClient.Bucket(bucketName)
}
