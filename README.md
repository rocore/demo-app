# Demo App

This demo application illustrates using an ephemeral environment for acceptance testing.

## Application

A simple app that accepts a message, stores it in a bucket and then sends the message on a PubSub topic.

## App Infrastructure

Infrastructure specific to the application that is contained in the `/infrastructure` directory. [Pulumi](pulumi.com) is used to manage our infrastructure.

## Acceptance Tests

To run our acceptance tests, we spin up an ephemeral environment. The simple example test is written in Go and contained in the `/acceptance` directory. The acceptance tests are run as a Kubernetes Job that is defined the `/infrastructure/acceptance.ts` file.

The tests check:

* When we hit the `/message` endpoint, we get back the expected status code that the same message is stored in the appropriate bucket as well as published to the appropriate PubSub topic.
* When we hit the `/message` endpoint with an incorrect payload, we get back a 400.
