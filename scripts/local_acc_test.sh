#!/bin/bash

set -e

docker build -t rocore/demo-app:testing-local .
docker push rocore/demo-app:testing-local

docker build -t rocore/demo-app-acceptance:testing-local ./acceptance
docker push rocore/demo-app-acceptance:testing-local

cd infrastructure
pulumi up -s rocore/local-test -y