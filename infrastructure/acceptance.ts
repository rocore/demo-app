import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as svcKey from "./svcKey";

interface AcceptanceConfig {
    env: string;
    project: string;
    topic: gcp.pubsub.Topic;
    bucket: gcp.storage.Bucket;
    accSvcAccountEmail: string;
    accSvcAccountId: string;
    dockerTag: string;
    appDeployment: k8s.apps.v1.Deployment;
    appService: k8s.core.v1.Service;
    clusterProvider: k8s.Provider;
}

export function setupAcceptanceTests(config: AcceptanceConfig): k8s.batch.v1.Job {
    const name = `${config.env}-demo-app-acc-test`;

    // Create subscription on the app's topic and give acceptance test svc
    // account permission to subscribe to the subscription.
    const subscription = new gcp.pubsub.Subscription(name, {
        topic: config.topic.name,
    });
    new gcp.pubsub.SubscriptionIAMMember(name, {
        member: `serviceAccount:${config.accSvcAccountEmail}`,
        role: "roles/pubsub.subscriber",
        subscription: subscription.name,
    });

    // Give acceptance test svc account object admin permissions for bucket.
    // This is required so the acceptance test can delete the object created
    // during our tests and therefore we can cleanly delete the bucket.
    new gcp.storage.BucketIAMMember(name, {
        member: `serviceAccount:${config.accSvcAccountEmail}`,
        role: "roles/storage.objectAdmin",
        bucket: config.bucket.name,
    });

    // Create a service account key for the acceptance test service account
    // and then use that key to create a K8s secret that will be mounted to
    // our K8s job.
    const serviceAccountKey = svcKey.getSvcKey(name, config.accSvcAccountId);
    const secret = new k8s.core.v1.Secret(name, {
        metadata: {
            name: `${config.env}-google-cloud-key-acc-test`,
        },
        stringData: { "key.json": serviceAccountKey },
    }, { provider: config.clusterProvider });

    // Create the job for our acceptance tests. The job will only run one time.
    // By default, `pulumi up` will wait for the job to complete and only succeed
    // if the container (aka our acceptance tests) exit successfully. The means we
    // can simply run `pulumi up` to run our acceptance tests and determine if they
    // succeeded or not.
    const jobLabels = { appClass: name };
    return new k8s.batch.v1.Job(name, {
        metadata: { labels: jobLabels },
        spec: {
            template: {
                metadata: { labels: jobLabels },
                spec: {
                    containers: [{
                        name: name,
                        image: `rocore/demo-app-acceptance:${config.dockerTag}`,
                        imagePullPolicy: "Always",
                        env: [
                            { name: "SUBSCRIPTION", value: subscription.name },
                            { name: "BUCKET", value: config.bucket.name },
                            { name: "PROJECT", value: config.project },
                            { name: "SERVICE_NAME", value: pulumi.interpolate`${config.appService.metadata.name}` },
                            { name: "GOOGLE_APPLICATION_CREDENTIALS", value: "/var/secrets/google/key.json" },
                        ],
                        volumeMounts: [
                            { name: "google-cloud-key", mountPath: "/var/secrets/google" }
                        ]
                    }],
                    volumes: [{
                        name: "google-cloud-key",
                        secret: { secretName: secret.metadata.name }
                    }],
                    restartPolicy: "Never",
                },
            },
            backoffLimit: 0,
        },
    }, {
        provider: config.clusterProvider,
        customTimeouts: { create: "5m" },
        dependsOn: [config.appDeployment]
    });
}
