import * as gcp from "@pulumi/gcp";
import * as pulumi from "@pulumi/pulumi";

export function getSvcKey(name: string, svcAccountId: string): pulumi.Output<string> {
    // Create service account key.
    const serviceAccountKey = new gcp.serviceAccount.Key(name, {
        serviceAccountId: svcAccountId,
    })

    // Decode key.
    return serviceAccountKey.privateKey.apply(key => {
        return Buffer.from(key, 'base64').toString('binary');;
    });
}