import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import * as acceptance from "./acceptance";
import * as svcKey from "./svcKey";

// Get GCP project configuration value. This allows us to set a "test"
// GCP project for our test resources and then a separate GCP project
// for production.
const gcpConfig = new pulumi.Config("gcp");
const project = gcpConfig.require("project");

// Get stack's configuration values. Using a configurable environment name,
// we can make multiple test resources in the same project without collisions.
// We use a configurable DOCKER_TAG to ensure we are testing the same immutable
// image that we will also deploy to production.
const config = new pulumi.Config("demo-app");
const ENV = config.require("ENV");
const DOCKER_TAG = config.require("DOCKER_TAG");

// Get the global stack reference and its outputs. These are our long-lived
// pieces of infrastructure such as our Kubernetes cluster and two service
// accounts. We have one service account that our application will use and
// one that our acceptance tests will use.
const globalStackRef = new pulumi.StackReference("rocore/global-infra/global-infra");
const kubeconfigOutput = globalStackRef.getOutput("kubeconfig");
const namespaceOutput = globalStackRef.getOutputSync("namespace");
const accTestSvcAccount = globalStackRef.getOutputSync("acceptanceTestSvcAccount");

// By default, we're going to use a "test" service account for our application.
// If the environment is "prod", we will use the production service account.
let svcAccountOutput = globalStackRef.getOutputSync("testSvcAccount");
if (ENV === "prod") {
    svcAccountOutput = globalStackRef.getOutputSync("prodSvcAccount");
}

// We prepend the environment to the name used for all resources. This
// allows us to run this same program multiple times for different test
// environments and not have resources with the duplicate names.
const name = `${ENV}-demo-app`;

// Create egress topic and give the app's service account permission
// to publish to the topic.
export const topic = new gcp.pubsub.Topic(name);
new gcp.pubsub.TopicIAMMember(name, {
    member: `serviceAccount:${svcAccountOutput.email}`,
    role: "roles/pubsub.publisher",
    topic: topic.name,
}, { dependsOn: [topic] });

// Create bucket and give the app's service account permission to
// create objects in the bucket.
export const bucket = new gcp.storage.Bucket(name, {
    location: "us-west1",
});
new gcp.storage.BucketIAMMember(name, {
    member: `serviceAccount:${svcAccountOutput.email}`,
    role: "roles/storage.objectCreator",
    bucket: bucket.name,
});

// Create a Kubernetes provider from the global stack reference.
// We will use this make sure our K8s resources are created in the
// correct cluster and namespace.
const clusterProvider = new k8s.Provider(name, {
    kubeconfig: kubeconfigOutput,
    namespace: namespaceOutput.metadata.name,
});

// Create a service account key for the app's service account
// and then use that key to create a K8s secret that will be mounted to
// our K8s deployment.
const serviceAccountKey = svcKey.getSvcKey(name, svcAccountOutput.id);
const secret = new k8s.core.v1.Secret(name, {
    metadata: { name: `${ENV}-google-cloud-key` },
    stringData: { "key.json": serviceAccountKey },
}, { provider: clusterProvider });

// Create a K8s Deployment for our application.
const appLabels = { appClass: name };
const deployment = new k8s.apps.v1.Deployment(name, {
    metadata: { labels: appLabels },
    spec: {
        selector: { matchLabels: appLabels },
        template: {
            metadata: { labels: appLabels },
            spec: {
                containers: [{
                    name: name,
                    image: `rocore/demo-app:${DOCKER_TAG}`,
                    imagePullPolicy: "Always",
                    ports: [{ name: "http", containerPort: 8080 }],
                    env: [
                        { name: "TOPIC", value: topic.name },
                        { name: "BUCKET", value: bucket.name },
                        { name: "PROJECT", value: project },
                        {
                            name: "GOOGLE_APPLICATION_CREDENTIALS",
                            value: "/var/secrets/google/key.json"
                        },
                    ],
                    volumeMounts: [{
                        name: "google-cloud-key",
                        mountPath: "/var/secrets/google"
                    }],
                    readinessProbe: {
                        httpGet: { path: "/ping", port: 8080 },
                        initialDelaySeconds: 3,
                        periodSeconds: 3,
                    },
                }],
                volumes: [{
                    name: "google-cloud-key",
                    secret: { secretName: secret.metadata.name },
                }]
            }
        }
    },
}, { provider: clusterProvider });

// Create a ClusterIP Service for the K8s Deployment.
const service = new k8s.core.v1.Service(name, {
    metadata: { labels: appLabels },
    spec: {
        ports: [{ port: 80, targetPort: 8080 }],
        selector: appLabels,
    },
}, { provider: clusterProvider });

// If it's a test environment, set up acceptance tests.
let job: k8s.batch.v1.Job | undefined;
if (ENV.startsWith("test")) {
    job = acceptance.setupAcceptanceTests({
        env: ENV,
        project: project,
        topic: topic,
        bucket: bucket,
        accSvcAccountEmail: accTestSvcAccount.email,
        accSvcAccountId: accTestSvcAccount.id,
        dockerTag: DOCKER_TAG,
        appDeployment: deployment,
        appService: service,
        clusterProvider: clusterProvider,
    });
}

// Export the acceptance job name, so we can get the logs from our
// acceptance tests.
export const acceptanceJobName = job ? job.metadata.name : "unapplicable";
